//[SECTION] Creating A Basic Server Setup

//Node JS => Will provide us with a "runtime environment (RTE)" that will allow us to execute our program/application. 

//RTE (Runtime Environment) -> we are pertaining to an environment/system in which a program can be executed. 

//TASK: let's create a standard server setup using 'plain' NodeJS. 

//1. Identify and prepare the components/Ingredients that you would need in order to execute the task. 

     //the main ingredient when creating a server using plain Node is:  *http* (hypertext transfet protocol). 
     //http -> is a 'built in' module of NODE JS, that will allow us to 'establish a connnection' and will make it possible to transfer data over HTTP. 

     //you need to be able to get the components first, 

     //we will use the require() directive -> this is a function that will allow us to gather and acquire certain packages that we will use to build our application.  
const http = require('http'); 
     //http will provide us with all the components needed to establish a server.   

     //http contains the utility constructor called 'createServer()' -> which will allow us to create an HTTP server. 

//2. Identify and describe a location where the connection will happen. in order to provide a proper medium for both parties (client and server), we will then bind the connection to the desired port number. 
let port = 3000; 

//3. Inside the server constructor, insert a method in which we can use in order describe the connect that was established. identify the interaction between the client and the server and pass them down as the arguments of the method.


http.createServer((request, response) => {

 //4. bind/assign the connection to the address, the listen() is used to bind and listen to a specific port whenever its being access by the computer. 

 //write() -> this will allow to insert messages or input to our page. 
   response.write(`Welcome to Port ${port}`); 
   response.write('Hello Batch 165'); 
   response.write('make me appear to the page'); 
   response.end(); //end() will allow us to identify a point where the transmission of data will end. 
}).listen(port); 

console.log('Server is Running'); 
